import React, { Component } from "react";
import SecuritiesNavBar from "./SecuritiesNavBar";

const Home = props => {
  return (
    <div>
      <SecuritiesNavBar />
      <h2
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh"
        }}
      >
        Securities Management Application
      </h2>
    </div>
  );
};

export default Home;
