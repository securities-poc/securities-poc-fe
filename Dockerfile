#FROM node:14.4.0-alpine3.12
FROM node:12.18.0-alpine3.12

# create new user
#RUN useradd -ms /bin/bash appuser
RUN adduser -D appuser
# change user
USER appuser
# set container working dir to user home dir
WORKDIR /home/appuser

COPY package.json .
RUN npm install
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]