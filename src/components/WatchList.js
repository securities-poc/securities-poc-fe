import React, { Component } from "react";
import SecuritiesNavBar from "./SecuritiesNavBar";
import { Table } from "reactstrap";

class WatchList extends Component {
  state = {};
  render() {
    return (
      <div>
        <SecuritiesNavBar />
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>Name</th>
              <th>Quantity</th>
              <th>Current Price</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Microsoft</td>
              <td>1</td>
              <td>140.52</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

export default WatchList;
