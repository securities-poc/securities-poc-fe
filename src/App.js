import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import Portfolios from "./components/Portfolios";
import WatchList from "./components/WatchList";
import Home from "./components/Home";
function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact={true} component={Home} />
        <Route path="/Portfolios" exact={true} component={Portfolios} />
        <Route path="/WatchList" exact={true} component={WatchList} />
      </Switch>
    </Router>
  );
}

export default App;
