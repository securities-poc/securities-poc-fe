export default interface IpoCalendar {
  date: string;
  exchange?: string;
  name: string;
  numberOfShares?: number;
  price?: string;
  status: string;
  symbol?: string;
  totalSharesValue?: number;
}
