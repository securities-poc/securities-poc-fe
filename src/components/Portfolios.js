import React, { Component, useEffect } from "react";
import SecuritiesNavBar from "./SecuritiesNavBar";
import Table from "react-bootstrap/Table";

class PortfoliosContainer extends Component {
  state = {
    isLoading: true,
  };

  //called immediately after a component is mounted
  //side-effects
  async componentDidMount() {
    fetch("/mock/api/portfolios")
      .then((response) => response.json())
      .then(
        (data) => this.setState({ isLoading: false, data }),
        (error) => this.setState({ isLoading: false, error })
      );
  }

  render() {
    return <PortfoliosView {...this.state} />;
  }
}

class PortfoliosView extends Component {
  render() {
    if (this.props.isLoading) {
      return this.renderLoading();
    } else if (this.props.data) {
      return this.renderPortfolios();
    } else {
      return this.renderError();
    }
  }

  renderPortfolios() {
    const { size, portfolios } = this.props.data;
    return (
      <div>
        <SecuritiesNavBar />
        <h4 style={{ alignItems: "center" }}>Portfolios</h4>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh",
          }}
        >
          <Table striped bordered hover size="sm">
            <thead>{size} Portfolios</thead>
            <tbody>
              {portfolios.map((pf) => (
                <tr>
                  <td align="left">{pf.portfolioName}</td>
                  <td align="left">{pf.accountType}</td>
                  <td align="right">{pf.totalValue}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }

  renderLoading() {
    return (
      <div>
        <SecuritiesNavBar />
        <h4 style={{ alignItems: "center" }}>Loading security portfolios..</h4>
      </div>
    );
  }

  renderError() {
    return (
      <div>
        <SecuritiesNavBar />
        <h4 style={{ alignItems: "center" }}>Unable to load portfolios. Please try again</h4>
      </div>
    );
  }
}

export default PortfoliosContainer;
