import IpoCalendar from "./IpoCalendar.interface";

export default interface Ipos {
  ipoCalendar: IpoCalendar[];
}
