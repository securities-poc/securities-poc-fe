import React, { PureComponent } from "react";
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from "reactstrap";
const SecuritiesNavBar = props => {
  return (
    <div>
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">Securities</NavbarBrand>
        <Nav className="mr-auto" navbar>
          <NavItem>
            <NavLink href="/Portfolios">My Portfolios</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/WatchList">My Watchlist</NavLink>
          </NavItem>
        </Nav>
      </Navbar>
    </div>
  );
};

export default SecuritiesNavBar;
